conda create -n parallel-workshop python=3.7 -y
conda activate parallel-workshop
conda install -c conda-forge python-graphviz -y
pip install bokeh dask distributed jupyter