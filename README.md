# Parallel Python

A workshop for scientists who've seen Python before and want to parallelize
their code and/or deploy it on a cluster.

Jump to section:  
* [Background](#background)
* [Setting up your computer](#setting-up-your-computer)
* [Pre-requisites](#pre-requisites)
* [Workshop video](#workshop-video)

## Background

The workshop is presented by [Jenni Rinker](https://www.linkedin.com/in/jennirinker)
and is based off of her lecture at the 2019 summer school in
[Advanced Scientific Python Programming](https://python.g-node.org).
The materials for the ASPP lecture can be found 
[here on GitHub](https://github.com/ASPP/2019-camerino-parallel-python).

## Setting up your computer

These instructions assume you alread have Anaconda installed (preferrably with
Python 3.X).

Before you run the exercises, you should first download the workshop materials 
and set up an environment to isolate the code. Instructrions to do this are
given in the subsection below.

**Windows users**, in order to properly visualize the task graphs in some of the
exercises, you need to configure a program called `graphviz`. Thus, please also
follow the extra instructions in the subsection below titled
"Configure graphviz (Windows users)". If you don't configure graphviz, any time
you try to use the `visualize` function, you will get an error.

### Download materials and set up environment (all users)

Open an Anaconda prompt. In the prompt, enter the instructions in the block
below, where `<path>` is the path to the directory where you want the workshop
folder to be created (e.g., `C:\Users\rink\Documents`).  
```
cd <path>
git clone git clone https://gitlab.windenergy.dtu.dk/python-at-risoe/scientific-python-workshops/parallel-python.git
cd parallel-python
conda create -n parallel-workshop python=3.7 -y
conda activate parallel-workshop
conda install -c conda-forge python-graphviz -y
pip install bokeh dask distributed jupyter
```

Here is an explanation of each line:  
1. Changes to the directory where you want the workshop folder to be.
1. Clones the workshop materials into a new folder `parallel-python`.
1. Changes into the new folder.
1. Create a conda environment for the workshop.
1. Activate the environment.
1. Install graphviz from conda forge.
1. Install several needed packages from PyPI.

### Configure graphviz (Windows users)

At the time of this writing, Windows users need to manually configure `graphviz`
in order to visualize task graphs. 

To configure graphviz, please do the following:  
1. Close all running Python kernels (e.g., Jupyter notebooks, Spyder, etc.).
1. Install graphviz using the Windows installer by downloading and running the
   `.msi` file at [this link](https://graphviz.gitlab.io/_pages/Download/Download_windows.html).
1. Add the graphviz bin folder (e.g., `C:\Program Files (x86)\Graphviz2.38\bin`)
   to your Windows Path variable. You can find more detailed instructions on
   how to add something to your path by Googling.

## Pre-requisites

* **Python/software knowledge**: You must be at least a little familiar with
  Python syntax and importing packages, but you don't need to be an expert.
  Familiarity with git, Anaconda and Jupyter notebooks will help you, but is not
  necessary.
* **General knowledge**: No previous knowledge of computer architecture or 
  parallelization is required.

## Workshop video

The workshop was filmed and will be soon placed on YouTube, so you can see the
lecture on your own time. The link will be added here when the video has been
posted.

